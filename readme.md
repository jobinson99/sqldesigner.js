sqldesigner.js
==============
databasetoolkit
db.tk

本项目是从 [wwwsqldesigner](http://code.google.com/p/wwwsqldesigner) 分支出来的，原有的中文翻译和sqlalchemy是本人所改写，但在新版都被遗弃。

我的目标：
----------

1. 适应更多浏览器和屏幕大小，触控操作。
2. 使用canvas改写绘制界面，以适应大规模数据结构需求。
3. 适应更多数据库。
4. 更多输出方式。

orm:sequelize.js

交叉生成：

数据库  orm  开发语言
     sqlarchemy python
     sequeelize js

数据库 开发语言
MySQL
MariaDB
SQLite
PostgreSQL
MS SQL
Oracle
MongoDB



wwwsqldesigner的说明
--------------------

For documentation, visit http://code.google.com/p/wwwsqldesigner/w/list
WWW SQL Designer allows users to create database designs, which can be saved/loaded and exported to SQL scripts. Various databases and languages are supported. Ability to import existing database design.

[YouTube video](http://www.youtube.com/watch?v=hCQzJx9AKhU)

### About ###

Hi and welcome to WWW SQL Designer! This tool allows you to draw and create database schemas (E-R diagrams) directly in browser, without the need for any external programs (flash). You only need JavaScript enabled. The Designer works perfectly in Mozillas (Firefox, Seamonkey), Internet Explorers (6, 7, 8), Safari and Operas. Konqueror works, but the experience is limited.

Many database features are supported, such as keys, foreign key constraints, comments and indexes. You can either save your design (for further loading & modifications), print it or export as SQL script. It is possible to retrieve (import) schema from existing database.

WWW SQL Designer was created by [Ondrej Zara](http://ondras.zarovi.cz/) and is built atop the [oz.js](http://code.google.com/p/oz-js/) JavaScript module. It is distributed under New BSD license.

If you wish to support this project, Donate at PayPal at PayPal!

### News ###

Experimental real-time collaboration version

Thanks to [Bharat Patil](http://bharat.whiteboard.jit.su/)

### New release ###

Version 2.7 was released on 3.8.2012. This is mainly a bugfix release, although several new features (most notable localStorage support) are present.

Release

Version 2.6 was released on 22.9.2011. Several new translations (pt\_BR, sv, ar) added; VML removed; new visualization options available (show length and datatype); new DBs and backends; support for touch devices...
Experimental clone with deletion of saved designs

http://code.google.com/r/charlieyouakim-wwwsqldesigner-deleteadd/
New optional patch

A new patch, which enable optional display of field details, was submitted by Wilson Oliveira. While this code is not ready yet to be commited into repository, everyone can download it from http://ondras.zarovi.cz/sql/wwwsqldesigner-inline_field_details_patch.zip.

Support for CUBRID

WWW SQL Designer now supports the [CUBRID database](http://www.cubrid.org/): both as a backend (using PHP) as well as datatype definition set.

Release

Version 2.5 was released on 18.6.2010. Many new features were added (hiding of sidebar, colored relation, multi-selection and multi-drag, ...), tons of bugs were fixed.

Release

Version 2.4 was released on 5.11.2009. Several outstanding issues were fixed and new locales added.

Release

Version 2.3.3 was released on 28.7.2009. This long-awaited release includes numerous fixes, compatibility improvements, new locales, backends and DB datatypes.

Release

Version 2.3.2 was released on 8.1.2009. Apart from some traditional bugfixes and locales, a new functionality is introduced - the ability to mark foregin keys between existing table fields!

Google Code

### 翻头重写2.0 ###

The project was recently moved to Google Code hosting, which (amongst many other things) introduces Subversion hosting. Enjoy! ([The old website](http://ondras.zarovi.cz/sql/), will still exist for some time.)

2.0 is here

好的一面：I has many new features, including bezier connectors, support for various customizations, localization, options and more.

不好的一面：原有1.x使用的是prototype写的，2.0起，基于oz.js 重写，原有的本地化和 XSLT 模板不再可用，抱歉了！！！ 

